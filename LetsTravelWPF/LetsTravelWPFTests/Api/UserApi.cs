﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WpfApp1.Api;

using WpfApp1.Model;
using RestSharp;
using System.Runtime.Serialization.Json;
using System.IO;
using System;
using System.Text;
using System.Net;

namespace LetsTravelWPFTests.Api
{
    [TestClass]
    public class UserApiTest
    {
        [TestMethod]
        public async System.Threading.Tasks.Task RegisterTest()
        {
            UserApi a = new UserApi();
            string answ = await a.Register("vladdoom@gmail.com", "Vlad123!", "Vlad123!");
            Assert.AreEqual(HttpStatusCode.OK, answ);
        }

        [TestMethod]
        public async System.Threading.Tasks.Task LoginTest()
        {
            UserApi a = new UserApi();
            string answ = await a.Login("vladdoom@gmail.com", "Vlad123!");
            Assert.AreEqual(HttpStatusCode.OK, answ);
        }
        
        [TestMethod]
        public async System.Threading.Tasks.Task GetUserProfileTest()
        {
            UserApi a = new UserApi();
            var answ = await a.GetUserProfile();
            //Assert.AreEqual("Ok", answ);
        }
        [TestMethod]
        public async System.Threading.Tasks.Task PostUserProfileTest()
        {
            UserApi a = new UserApi();
            UserApis u = new UserApis();
          //  var answ = await a.PostUserProfile(u);
           // Assert.AreEqual("Ok", answ);
        }

        [TestMethod]
        public async System.Threading.Tasks.Task LogoutTest()
        {
            UserApi a = new UserApi();
            string answ = await a.Logout();
            Assert.AreEqual("Ok", answ);
        }

        [TestMethod]
        public async System.Threading.Tasks.Task RecoverPasswordTest()
        {
            UserApi a = new UserApi();
            string answ = await a.RecoverPassword("vladdoom@gmail.com");
            Assert.AreEqual("Ok", answ);
        }
    }
}
