﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WpfApp1.Model;
using WpfApp1.Api;
using RestSharp;
using System.Runtime.Serialization.Json;
using System.IO;
using System;
using System.Text;

namespace LetsTravelWPFTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
             Country p = new Country();
            p.Id = 90;
            p.Name = "popo";

            MemoryStream stream1 = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Country));
            ser.WriteObject(stream1, p);
            stream1.Position = 0;
            StreamReader sr = new StreamReader(stream1);
            Console.Write("JSON form of Person object: ");
            Console.WriteLine(sr.ReadToEnd());
            byte[] json = stream1.ToArray();
            string trueee = "{" + '"' + "Name" + '"' + ':' + '"' + "popo" + '"' + '}';

            var request = new RestRequest("trip/", Method.POST);
            request.AddBody(Encoding.UTF8.GetString(json, 0, json.Length));

            Assert.AreEqual(trueee, Encoding.UTF8.GetString(json, 0, json.Length));
        }

        public void ContryToCountryApi() {
         
        }
    }
}
