﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using LetsTravel.WPF.ViewModel.Command;

namespace LetsTravel.WPF.ViewModel
{
    class MainViewModel
    {
        public System.Windows.Input.ICommand ConvertTextCommand
        {
            get { return new DelegateCommand(ConvertText); }
        }


        private void MenuLogIn_Click(object sender, RoutedEventArgs e)
        {
            Login login = new Login();
            login.ShowDialog();
            if ((bool)login.DialogResult) { LoginIndicatingMenu.Header = login.textBoxEmail.Text + "  loguot"; }

        }
        private async void MenuLogOut_Click(object sender, RoutedEventArgs e)
        {
            //  MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + " has been activated!");
            var a = await StaticVar.UserApi.Logout();
        }

        private void MenuRegister_Click(object sender, RoutedEventArgs e)
        {
            Registration registration = new Registration();
            registration.ShowDialog();
        }
        private void MenuRecoverPassword_Click(object sender, RoutedEventArgs e)
        {
            RecoverPassword recover_pass = new RecoverPassword();
            recover_pass.ShowDialog();
        }
        private void MenuUserProfile_Click(object sender, RoutedEventArgs e)
        {
            UserProfile user_profile = new UserProfile();
            user_profile.ShowDialog();
            //var userProfile = GlobalVar.StaticVar.UserApi.GetUserProfile();
            //MessageBox.Show(userProfile.ToString());

        }
        private void MenuCheckForUpdates_Click(object sender, RoutedEventArgs e)
        {
            Api.GoogleApi g = new GoogleApi();
            g.ReadAll("lviv opera");
            //Show the message box with the name of the current method
            // MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + " has been activated!");
        }
        private void MenuAbout_Click(object sender, RoutedEventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }
        private void MenuPreferences_Click(object sender, RoutedEventArgs e)
        {
            //Show the message box with the name of the current method
            MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + " has been activated!");
        }
        private void MenuExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void BuildTripButton_Click(object sender, RoutedEventArgs e)
        {
            this.contentMain.Content = new UC_CreateTrip();
        }
        private void ChooseTripButton_Click(object sender, RoutedEventArgs e)
        {
            this.contentMain.Content = new UC_ChooseTrip();
        }

    }
}
