﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LetsTravel.WPF.ViewModel
{
    class LoginViewModel
    {
        private async void LogInButton_Click(object sender, RoutedEventArgs e)
        {

            if (textBoxEmail.Text.Length == 0)
            {
                errormessage.Text = "Enter an email.";
                textBoxEmail.Focus();
            }
            else if (!Regex.IsMatch(textBoxEmail.Text, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"))
            {
                errormessage.Text = "Enter a valid email.";
                textBoxEmail.Select(0, textBoxEmail.Text.Length);
                textBoxEmail.Focus();
            }
            else
            {
                string email = textBoxEmail.Text;
                string password = passwordBox1.Password;

                LogInWithUserRecord(email);
            }
            LogInButton.IsEnabled = false;
            errormessage.Text = "Please wait while server verifying your login/password";
            var answerServer = await StaticVar.UserApi.Login(textBoxEmail.Text, passwordBox1.Password);
            if (answerServer.Equals("OK"))
            {
                Close();
            }
            else
            {
                errormessage.Text = answerServer;
                LogInButton.IsEnabled = true;
            }
        }

        private void LogInWithUserRecord(string userId)
        {
            //throw new NotImplementedException();
        }

        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            Registration registration = new Registration();
            Close();
            registration.ShowDialog();
        }
        private void RecoverPasswordButton_Click(object sender, RoutedEventArgs e)
        {
            RecoverPassword recover_password = new RecoverPassword();
            Close();
            recover_password.ShowDialog();
        }
        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            textBoxEmail.Text = "";
            passwordBox1.Password = "";
            errormessage.Text = "";
        }
    }
}
