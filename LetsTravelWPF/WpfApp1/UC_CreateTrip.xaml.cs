﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1.GlobalVar;
using WpfApp1.Model;
namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for UC_CreateTrip.xaml
    /// </summary>
    public partial class UC_CreateTrip : UserControl
    {
        public UC_CreateTrip()
        {
            InitializeComponent();
            //file map.html has to be placed into the startup directory
            /*string borwserPath = "pack://siteoforigin:,,,/map.html";
            Boolean noInternetConnection = false;
            if (noInternetConnection)
                borwserPath = "pack://siteoforigin:,,,/BrowserDefaultDisplay.png";
            this.browser.Navigate(new Uri(borwserPath));
            browser.BeginInit();
            */
        }
        private void CreatePlaceButton_Click(object sender, RoutedEventArgs e)
        {
            this.Content = new UC_CreatePlace();
        }
        private async void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            var listCity = await StaticVar.UniversalApi.ReadAll<City>("City");
            //foreach (City c in listCity) {
            //    if (c.Name.Equals(textBoxSearch.Text)) { }
            //}
            //this.LabelRight.Content = "Dynamic list of found places";            
            this.innerContentControl.Content = new UC_TripConstruct();
        }
        private void BackToStartButton_Click(object sender, RoutedEventArgs e)
        {
            this.Content = new UC_StartView();
        }

        private void ShowTripOnMapButton_Click(object sender, RoutedEventArgs e)
        {
            this.innerContentControl.Content = new UC_PreviewTrip();
            //change the label on the button -> click on it second time to submit the trip
            this.ShowTripOnMapButton.Content = "Submit trip";
            var clickedSecondTime = false;
            if (clickedSecondTime)
            {
                SubmitTrip();
            }                
        }
        private void SubmitTrip()
        {
            //implement method for submssion of the designed trip
            throw new NotImplementedException();
        }
    }
}
