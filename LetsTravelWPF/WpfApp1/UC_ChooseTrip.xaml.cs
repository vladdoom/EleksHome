﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for UC_ChooseTrip.xaml
    /// </summary>
    public partial class UC_ChooseTrip : UserControl
    {
        public UC_ChooseTrip()
        {
            InitializeComponent();
        }

        private void BackToStartButton_Click(object sender, RoutedEventArgs e)
        {
            this.Content = new UC_StartView();
        }

        private void CheckAvailabilityButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            this.Label1.Content = "Dynamic list of found places";
        }

        private void DescriptionLink_Click(object sender, RoutedEventArgs e)
        {
            TripDescription tripDescr = new TripDescription();
            tripDescr.ShowDialog();
        }
    }
}
