﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for UserProfile.xaml
    /// </summary>
    public partial class UserProfile : Window
    {
       
        public UserProfile()
        {
            InitializeComponent();
             ReadUserProfile();
            GetProfile();
        }

        private async void GetProfile() {
         var userProfile =  await  GlobalVar.StaticVar.UserApi.GetUserProfile();
            //textBoxFirstName.Text = userProfile.FirstName;
            //textBoxMiddleName.Text = userProfile.MiddleName;
            //textBoxLastName.Text = userProfile.LastName;
            //textBoxGender.Text = userProfile.IsMan.ToString(); //get a drop down list
            //BirthDate.SelectedDate = userProfile.BirthDate;
            //textBoxInterestedIn.Text = userProfile.PreferableThings;
            //textBoxAddress.Text = userProfile.Address.ToString();
            //textBoxPhone1.Text = userProfile.PhoneNumber1;
            //textBoxPhone2.Text = userProfile.PhoneNumber2;
            //textBoxLogin.Text = userProfile.Login;
            //textBoxEmail.Text = userProfile.Email;
            //passwordBox1.Password = userProfile.Password;
            //passwordBoxConfirm.Password = "";
        }
        private void ReadUserProfile()
        {
            textBoxFirstName.Text = "FirstName";
            textBoxMiddleName.Text = "MiddleName";
            textBoxLastName.Text = "LastName";
            textBoxGender.Text = "Gender"; //get a drop down list
            BirthDate.SelectedDate = DateTime.Today;
            textBoxInterestedIn.Text = "list of things";
            textBoxAddress.Text = "1 Main st., Lviv 79000, Ukraine";
            textBoxPhone1.Text = "+1-123-456-7890";
            textBoxPhone2.Text = "+1-123-456-7891";
            textBoxLogin.Text = "Default LogIn";
            textBoxEmail.Text = "some.name@email.com";
            passwordBox1.Password = "password";
            passwordBoxConfirm.Password = "password";
            //throw new NotImplementedException();
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (passwordBox1.Password.Equals(passwordBoxConfirm.Password)) {
                Model.UserApis userApi = new Model.UserApis();
              
            userApi.FirstName = textBoxFirstName.Text;
                userApi.MiddleName = textBoxMiddleName.Text;
                userApi.LastName = textBoxLastName.Text;
                userApi.IsMan = true;//Boolean.Parse( textBoxGender.Text); //get a drop down list
                userApi.BirthDate = (DateTime)BirthDate.SelectedDate;
                userApi.PreferableThings = textBoxInterestedIn.Text;
                //userApi.Address = new  textBoxAddress.Text;
                userApi.PhoneNumber1 = textBoxPhone1.Text;
                userApi.PhoneNumber2 = textBoxPhone2.Text;
                userApi.Login = textBoxLogin.Text;
                userApi.Email = textBoxEmail.Text;
                userApi.Password = passwordBox1.Password;
               string answer = await GlobalVar.StaticVar.UserApi.PostUserProfile(userApi, 1);
            }
        }
    }
}
