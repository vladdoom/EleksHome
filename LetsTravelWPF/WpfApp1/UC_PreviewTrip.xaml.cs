﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for UC_PreviewTrip.xaml
    /// </summary>
    public partial class UC_PreviewTrip : UserControl
    {
        public UC_PreviewTrip()
        {
            InitializeComponent();
            //file map.html has to be placed into the startup directory
            string borwserPath = "pack://siteoforigin:,,,/map.html";
            Boolean noInternetConnection = false;
            if (noInternetConnection)
                borwserPath = "pack://siteoforigin:,,,/BrowserDefaultDisplay.png";
            this.browser.Navigate(new Uri(borwserPath));
            browser.BeginInit();            
        }

        private void SubmitTripButton_Click(object sender, RoutedEventArgs e)
        {

        }
        private void DescriptionLink_Click(object sender, RoutedEventArgs e)
        {
            PlaceDescription placeDescr = new PlaceDescription();
            placeDescr.ShowDialog();
        }
    }
}
