﻿using System;
using System.Collections.Generic;

namespace WpfApp1.Model
{
    class Place
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Photo { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double DurationInSeconds { get; set; }
        public string WorkingHours { get; set; }
        public int CityId { get; set; }
        public virtual City City { get; set; }
        public virtual List<Trip> TripList { get; set; }
        public Place() { }
        public Place(int id, string name, string description, string photo, double latitude, double longitude) {
            this.Id = id;
            this.Name = name;
            this.Description = description;
            this.Photo = photo;
            this.Latitude = latitude;
            this.Longitude = longitude;
        }
        public Place( string name, string description, string photo, double latitude, double longitude)
        {
            this.Name = name;
            this.Description = description;
            this.Photo = photo;
            this.Latitude = latitude;
            this.Longitude = longitude;
        }
        override
            public String ToString()
        { return Name + Description + Photo; }
    }
    
}
