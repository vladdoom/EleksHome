﻿using System;
using System.Collections.Generic;

namespace WpfApp1.Model
{
    class PlaceApi
    {
      
        public string Name { get; set; }
        public string Description { get; set; }
        public string Photo { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double DurationInSeconds { get; set; }
        public string WorkingHours { get; set; }
        public int CityId { get; set; }
        
    }
    
}
