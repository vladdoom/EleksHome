﻿using System;
using System.Collections.Generic;

namespace WpfApp1.Model
{
    class TripApi
    {
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string TripName { get; set; }
        public string UserLogin { get; set; }
        public string Description { get; set; }
        public DateTime StartTime { get; set; }
        public double DurationInSeconds { get; set; }
        public virtual List<Place> PlaceList { get; set; }
     }
}
