﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Model.OpenWeatherModels
{
    class OpenForecast
    {
        public class Coord
        {
            public double lon { get; set; }
            public double lat { get; set; }
        }

        public class City
        {
            public int id { get; set; }
            public string name { get; set; }
            public Coord coord { get; set; }
            public string country { get; set; }
            public int population { get; set; }
        }

        public class Temp
        {
            public double day { get; set; }
            public double min { get; set; }
            public double max { get; set; }
            public double night { get; set; }
            public double eve { get; set; }
            public double morn { get; set; }
        }

        public class Weather
        {
            public int id { get; set; }
            public string main { get; set; }
            public string description { get; set; }
            public string icon { get; set; }
        }

        public class List
        {
            public int dt { get; set; }
            public Temp temp { get; set; }
            public double pressure { get; set; }
            public double humidity { get; set; }
            public IList<Weather> weather { get; set; }
            public double speed { get; set; }
            public double deg { get; set; }
            public double clouds { get; set; }
            public double? rain { get; set; }
        }

        public class Example
        {
            public City city { get; set; }
            public string cod { get; set; }
            public double message { get; set; }
            public int cnt { get; set; }
            public IList<List> list { get; set; }
        }
    }
}
