﻿using System.Collections.Generic;

namespace WpfApp1.Model
{
    class State
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<City> CityList { get; set; }
        public int CountryId { get; set; }
        public virtual Country Country { get; set; }
    }
}
