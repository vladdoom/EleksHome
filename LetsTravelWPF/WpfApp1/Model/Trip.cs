﻿using System;
using System.Collections.Generic;

namespace WpfApp1.Model
{
    class Trip
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string TripName { get; set; }
        public string UserLogin { get; set; }
        public string Description { get; set; }
        public DateTime StartTime { get; set; }
        public double DurationInSeconds { get; set; }
        public virtual List<Place> PlaceList { get; set; }
        public virtual List<User> UserList { get; set; }
        public virtual List<Post> PostList { get; set; }
        public Trip() { }
        public Trip(int id, string countryName, string cityName, string tripName, string userLogin, 
            string description, DateTime startTime, double durationInSeconds) {
            this.Id = id;
            this.CountryName = countryName;
            this.CityName = cityName;
            this.TripName = tripName;
            this.UserLogin = userLogin;
            this.Description = description;
            this.StartTime = startTime;
            this.DurationInSeconds = durationInSeconds;
        }
        public Trip( string countryName, string cityName, string tripName, string userLogin,
           string description, DateTime startTime, double durationInSeconds)
        {
            this.CountryName = countryName;
            this.CityName = cityName;
            this.TripName = tripName;
            this.UserLogin = userLogin;
            this.Description = description;
            this.StartTime = startTime;
            this.DurationInSeconds = durationInSeconds;
        }
        override
            public String ToString()
        { return CountryName+ "__" + CityName+ "__" + TripName+ "__" + UserLogin+ "__" + Description+ "__"+StartTime+"__"+DurationInSeconds; }
    }
}
