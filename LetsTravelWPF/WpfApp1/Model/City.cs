﻿using System.Collections.Generic;

namespace WpfApp1.Model
{
    class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<Place> PlaceList { get; set; }
        public int StateId { get; set; }
        public virtual State State { get; set; }
    }
}
