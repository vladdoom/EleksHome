﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.CompilerServices;
using System.Windows;
using RestSharp;
using System.Net;
using System.Web;
[assembly: InternalsVisibleTo("LetsTravelWPFTests")]
namespace WpfApp1.Api
{
    class UserApi
    {


        async public Task<string> Login(string username, string password)
        {
            try
            {

                var client = new RestClient("https://localhost:44344");
                var request = new RestRequest("/Token", Method.POST);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("grant_type", "password");
                request.AddParameter("username", username);
                request.AddParameter("password", password);

                var response = await client.ExecuteTaskAsync(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var ar = response.Content.Split('"');
                    GlobalVar.StaticVar.AccessToken = ar[3];
                    MessageBox.Show(response.Content);
                }
                return response.StatusCode.ToString();
               
            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e.Message);
            }
            return "no connect to server";
        }
        async public Task<string> Logout()
        {
            try
            {
                var client = new RestClient("https://localhost:44344");
                var request = new RestRequest("/api/Account/Logout", Method.POST);
                request.AddHeader("Authorization", "Bearer " + GlobalVar.StaticVar.AccessToken);
                var response = await client.ExecuteTaskAsync(request);
                return response.StatusCode.ToString();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e.Message);
            }
            return "no connect to server";
        }

        async public Task<string> RecoverPassword(string password)
        {
            try
            {

                var client = new RestClient("https://localhost:44344");
                var request = new RestRequest("/api/Account/ForgotPassword", Method.POST);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("grant_type", "password");
                request.AddParameter("Password", password);

                var response = await client.ExecuteTaskAsync(request);
                return response.StatusCode.ToString();

            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e.Message);
            }
            return "no connect to server";
        }

        async public Task<string> Register(string username, string password, string confirmPassword)
        {
            try
            {

                var client = new RestClient("https://localhost:44344");
                var request = new RestRequest("/api/Account/Register", Method.POST);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("grant_type", "password");
                request.AddParameter("Email", username);
                request.AddParameter("Password", password);
                request.AddParameter("ConfirmPassword", confirmPassword);

                var response = await client.ExecuteTaskAsync(request);
                return response.StatusCode.ToString();

            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e.Message);
            }
            return "no connect to server";
        }


        async public Task<Model.User> GetUserProfile()
        {
            try
            {

                var client = new RestClient("https://localhost:44344");
                var request = new RestRequest("/odata/User", Method.GET);
                request.AddHeader("Authorization", "Bearer " + GlobalVar.StaticVar.AccessToken);
                 var response = await client.ExecuteTaskAsync< Model.User>(request);
                var dat = response.Data;
                MessageBox.Show("First message_"+dat.ToString()+dat.Id+dat.Email);                   
                MessageBox.Show(response.Content.ToString()  + response.Headers + response.StatusCode);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return null;}
                return null;

            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e.Message);
            }
            return null;
        }



        async public Task<string> PostUserProfile(Model.UserApis model, int id)
        {
            try
            {

                var client = new RestClient("https://localhost:44344");
                var request = new RestRequest("/odata/User("+id+')', Method.PUT);
                request.AddHeader("Authorization", "Bearer " + GlobalVar.StaticVar.AccessToken);
                request.AddJsonBody(model);

                var response = await client.ExecuteTaskAsync(request);
                
                return response.StatusCode.ToString();

            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e.Message);
            }
            return "no connect to server";
        }
    }
}

