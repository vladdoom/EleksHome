﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RestSharp;
using WpfApp1.Model;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("LetsTravelWPFTests")]

namespace WpfApp1.Api
{
    class UniversalApi
    {
        public async Task<string> SaveAsync<T>(T model)
        {
            try
            {
                var client = new RestClient("https://localhost:44344");
                var request = new RestRequest("/odata/Country", Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", "Bearer " + GlobalVar.StaticVar.AccessToken);
                request.AddJsonBody(model);
                var response = await client.ExecuteGetTaskAsync(request);
                return response.StatusCode.ToString();
                }
            catch (Exception e) { Console.Write(e.Message); }
            return "Error connect to server";
        }

        async   public Task<T>  ReadOne <T>(string nameModel,   int id){
            try
            {   
                var client = new RestClient("https://localhost:44344");
                var request = new RestRequest("/odata/"+nameModel+'(' + id + ')', Method.GET);
                request.AddHeader("Authorization", "Bearer " + GlobalVar.StaticVar.AccessToken);
                var response =await client.ExecuteGetTaskAsync<T>(request);

                if(response.StatusCode == HttpStatusCode.OK)
                { 
                T model = response.Data;
                return model;
                }
            }
            catch (Exception e) { }
            return default(T);
        }

        public async Task<List<T>> ReadAll <T>(string nameModel){
            try { 
                var client = new RestClient("https://localhost:44344");
        var request = new RestRequest("/odata/"+nameModel, Method.GET);
        request.AddHeader("Authorization", "Bearer " + GlobalVar.StaticVar.AccessToken);
                var response =await client.ExecuteGetTaskAsync<List<T>>(request);
                MessageBox.Show(response.Content+response.StatusCode);
        if(response.StatusCode == HttpStatusCode.OK)
                {
                        List<T> model = response.Data;
                        return model;
                    }
        }
            catch (Exception e) { }
            return default(List<T>);
    }

  public async Task<string>  Delete <T>(int id){
            try   {
                var client = new RestClient("https://localhost:44344");
                var request = new RestRequest("/odata/Country(" + id + ")", Method.GET);
                request.AddHeader("Authorization", "Bearer " + GlobalVar.StaticVar.AccessToken);
                var response = await client.ExecuteGetTaskAsync(request);
                return response.StatusCode.ToString();
            }
            catch (Exception e) { }
            return "Error connect to server";
        }

      public  async Task<string> Update <T>(T model) {

            try
            {
                var client = new RestClient("https://localhost:44344");
                var request = new RestRequest("/odata/Country", Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", "Bearer " + GlobalVar.StaticVar.AccessToken);
                request.AddJsonBody(model);
                var ii = client.Execute(request);//await
                var response =await client.ExecuteGetTaskAsync(request);
                //if{response.StatusCode=="200"}()
                MessageBox.Show(response.StatusCode + "__" + response.Content + "__");
                return response.StatusCode.ToString();

            }
            catch (Exception e) { Console.Write(e.Message); }
            return "Error conect to server";
        }

    }

}
