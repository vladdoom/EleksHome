﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;
using RestSharp;

namespace WpfApp1.Api
{
    class GoogleApi
    {
        string[] arrPlaceId;
        public async Task<string[]> ReadAll(string namePlace)
        {
            try
            {
                var client = new RestClient("https://maps.googleapis.com/");
                var request = new RestRequest("maps/api/place/autocomplete/json", Method.GET);
                request.AddParameter("input", namePlace);
                request.AddParameter("key", "AIzaSyAZ-GmyhcUl3lbW56fvhVnKW9JQ5qgM2nw");
                
                var response = await client.ExecuteGetTaskAsync(request);
                //var da =SimpleJson. response.Content;

                dynamic deserializedValue = JsonConvert.DeserializeObject(response.Content);
                var values = deserializedValue["predictions"];
                string s = "";
                string[] arr = new string[values.Count];
                arrPlaceId = new string[values.Count];
                for (int i = 0; i < values.Count; i++)
                {
                    Console.WriteLine("####"+values[i]["description"]);
                    arr[i] = values[i]["description"];
                    arrPlaceId[i] = values[i]["place_id"];
                }

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return arr;
                }
            }
            catch (Exception e) { }
            return null;
        }


        public async Task<string[]> getPlace(int number)
        {
            try
            {
                var client = new RestClient("https://maps.googleapis.com/");
                var request = new RestRequest("maps/api/place/autocomplete/json", Method.GET);
                request.AddParameter("placeid", arrPlaceId[number]);
                request.AddParameter("key", "AIzaSyAZ-GmyhcUl3lbW56fvhVnKW9JQ5qgM2nw");

                var response = await client.ExecuteGetTaskAsync(request);
                //var da =SimpleJson. response.Content;

                dynamic deserializedValue = JsonConvert.DeserializeObject(response.Content);
                var values = deserializedValue["predictions"];
                string s = "";
                string[] arr = new string[values.Count];
                for (int i = 0; i < values.Count; i++)
                {
                    Console.WriteLine("####" + values[i]["description"]);
                    arr[i] = values[i]["description"];

                }

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return arr;
                }
            }
            catch (Exception e) { }
            return null;
        }

    }
}
