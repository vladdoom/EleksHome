﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using WpfApp1.Api;
using WpfApp1.Model;
using WpfApp1.GlobalVar;
using Microsoft.AspNet.SignalR.Client;
using System.Net.Http;

namespace WpfApp1
{
    public partial class MainWindow : Window
    {
        public String UserName { get; set; }
        public IHubProxy HubProxy { get; set; }
        const string ServerURI = "https://localhost:44344/signalr/hubs";
        public HubConnection Connection { get; set; }

        public MainWindow()
        {
            InitializeComponent();


            this.contentMain.Content = new UC_StartView();


            var s = new SSLSertificat.SertificatCheck();

            InitialGlobalVariable();
        }

        public void DetermineLength(string message)
        {
            Console.WriteLine(message);
        }
        private void InitialGlobalVariable()
        {

            StaticVar.UniversalApi = new Api.UniversalApi();
            StaticVar.UserApi = new Api.UserApi();
        }

        private void MenuLogIn_Click(object sender, RoutedEventArgs e)
        {
            Login login = new Login();
            login.ShowDialog();
            if ((bool)login.DialogResult) { LoginIndicatingMenu.Header = login.textBoxEmail.Text + "  loguot"; }

        }
        private async void MenuLogOut_Click(object sender, RoutedEventArgs e)
        {
            var a = await StaticVar.UserApi.Logout();
        }

        private void MenuRegister_Click(object sender, RoutedEventArgs e)
        {
            Registration registration = new Registration();
            registration.ShowDialog();
        }
        private void MenuRecoverPassword_Click(object sender, RoutedEventArgs e)
        {
            RecoverPassword recover_pass = new RecoverPassword();
            recover_pass.ShowDialog();
        }
        private void MenuUserProfile_Click(object sender, RoutedEventArgs e)
        {
            UserProfile user_profile = new UserProfile();
            user_profile.ShowDialog();

        }
        private void MenuCheckForUpdates_Click(object sender, RoutedEventArgs e)
        {
        }
        private void MenuAbout_Click(object sender, RoutedEventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }
        private void MenuPreferences_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + " has been activated!");
        }
        private void MenuExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void BuildTripButton_Click(object sender, RoutedEventArgs e)
        {
            this.contentMain.Content = new UC_CreateTrip();
        }
        private void ChooseTripButton_Click(object sender, RoutedEventArgs e)
        {
            this.contentMain.Content = new UC_ChooseTrip();
        }

        public static bool HasConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = new WebClient().OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool CheckConnection(string URL)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Timeout = 5000;
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK) return true;
                else return false;
            }
            catch
            {
                return false;
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            WheatherWindow wheatherWindow = new WheatherWindow();
            wheatherWindow.ShowDialog();
        }






        private async void ConnectAsync()
        {
            Connection = new HubConnection(ServerURI);
            Connection.Closed += Connection_Closed;
            HubProxy = Connection.CreateHubProxy("MyHub");
            HubProxy.On<string, string>("AddMessage", (name, message) =>
                this.Dispatcher.Invoke(() =>
                    RichTextBoxConsole.AppendText(String.Format("{0}: {1}\r", name, message))
                )
            );
            try
            {
                await Connection.Start();
            }
            catch (HttpRequestException)
            {
                StatusText.Content = "Unable to connect to server: Start server before connecting clients.";
                return;
            }

            SignInPanel.Visibility = Visibility.Collapsed;
            ChatPanel.Visibility = Visibility.Visible;
            ButtonSend.IsEnabled = true;
            TextBoxMessage.Focus();
            RichTextBoxConsole.AppendText("Connected to server at " + ServerURI + "\r");
        }
        void Connection_Closed()
        {
            var dispatcher = Application.Current.Dispatcher;
            dispatcher.Invoke(() => ChatPanel.Visibility = Visibility.Collapsed);
            dispatcher.Invoke(() => ButtonSend.IsEnabled = false);
            dispatcher.Invoke(() => StatusText.Content = "You have been disconnected.");
            dispatcher.Invoke(() => SignInPanel.Visibility = Visibility.Visible);
        }

        private void SignInButton_Click(object sender, RoutedEventArgs e)
        {
            UserName = UserNameTextBox.Text;
            if (!String.IsNullOrEmpty(UserName))
            {
                StatusText.Visibility = Visibility.Visible;
                StatusText.Content = "Connecting to server...";
                ConnectAsync();
            }
        }

        private void WPFClient_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Connection != null)
            {
                Connection.Stop();
                Connection.Dispose();
            }
        }

        private void ButtonSend_Click(object sender, RoutedEventArgs e)
        {
            HubProxy.Invoke("Send", UserName, TextBoxMessage.Text);
            TextBoxMessage.Text = String.Empty;
            TextBoxMessage.Focus();
        }
    }
}